import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import { syncHistoryWithStore } from 'react-router-redux';
import { browserHistory } from 'react-router';
import rootReducer from '../reducers';

export const store = createStore(
	rootReducer,
	composeWithDevTools(
		applyMiddleware(
			thunk
		)
	)
);
export const history = syncHistoryWithStore(browserHistory, store);
