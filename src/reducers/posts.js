import { GET_POSTS, ADD_POST, DELETE_POST } from '../constants/constants';

import { getPosts } from '../actions/actions';
export default function posts(state = [], action) {
	switch(action.type){

		case GET_POSTS:
			return action.posts
			
		case ADD_POST:
			return [
        ...state,
        action.payload
      ]

		case DELETE_POST:
      return [
        ...state,
        action.payload
      ]

		default:
			return state
	}
}
