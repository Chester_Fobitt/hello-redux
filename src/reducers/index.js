import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import posts from './posts';
import filterPosts from './filterPosts';

const rootReducer = combineReducers({
  routing: routerReducer,
  posts,
  filterPosts
});

export default rootReducer;
