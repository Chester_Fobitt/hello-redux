const initialState = [];

export default function todos(state = initialState, action) {
	switch(action.type){
		case 'ADD_TODO':
			return [
        ...state,
        action.payload
      ]
    case 'DELETE_TODO':
      return [
        ...state,
        action.payload
      ]
		default:
			return state
	}
}
