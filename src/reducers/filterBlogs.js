const initialState = '';

export default function filterBlogs(state = initialState, action) {
	switch(action.type){
		case 'FIND_BLOG':
			return action.payload

		default:
			return state
	}
}
