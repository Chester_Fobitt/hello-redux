import { FIND_POST } from '../constants/constants'

export default function filterPosts(state = [], action) {
	switch(action.type){
		case FIND_POST:
			return action.payload

		default:
			return state
	}
}
