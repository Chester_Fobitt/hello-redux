import { GET_POSTS, AUTH_USER, SIGN_OUT_USER, FIND_POST } from '../constants/constants';
import { browserHistory } from 'react-router';
import database from './database';


export const getFindPosts = (name) => {
  return {
      type: FIND_POST,
      payload: name.toLowerCase()
  }
}

export const getPostsFulfilledAction = (posts) => {
  return {
    type: GET_POSTS,
    posts
  }
}

export const getPosts = () => dispatch => {
  return database.ref('/posts').once('value', result => {
    const posts = result.val();
    dispatch(getPostsFulfilledAction(posts))
  })
}

export const signOutUser = () => {
  browserHistory.push('/login');
  return {
    type: SIGN_OUT_USER
  }
}

export const signInUser = () => {
  browserHistory.push('/');
  return {
    type: AUTH_USER
  }
}

export const verifyAuth = () => {
  console.log('I`s verify!');
  return {
    type: SIGN_OUT_USER
  }
}
