import firebase from 'firebase';
import { API_URL, AUTH_DOMEN, API_KEY } from '../constants/constants';

const config = {
  apiKey: API_URL,
  authDomain: AUTH_DOMEN,
  databaseURL: API_KEY
}

firebase.initializeApp(config);
const database = firebase.database();

export default database;
