import React, { PureComponent } from 'react'
import {connect} from 'react-redux'
import { Link } from 'react-router';

class PostListItem extends PureComponent {
  render () {
    const {post, id} = this.props;

    return (
      <div className="blog__item">
        <h4>
          <Link to={`/posts/${id}`}>{post.title}</Link>
        </h4>
        <p className="content__text">
          {post.description}
        </p>
        <div className="share_likes clearfix">
          <div className="read-more ">
            <Link to={`/posts/${id}`} className="pure-button pure-button-primary">Read more</Link>
          </div>
          <div className="likes">
            <i className="fa fa-heart"></i>
            <span className="love-count">{post.likes}</span>
          </div>
        </div>
      </div>
    )
  }
}

export default connect()(PostListItem)
