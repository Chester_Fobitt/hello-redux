import React, { PureComponent } from 'react'
import PostListItem from './PostListItem'
// redux/firebase
import { connect } from 'react-redux'

class PostsList extends PureComponent  {

  render(){
    const { posts } = this.props;
    const postList = (posts.length) ? (
                                  Object.keys(posts).map((key) => (
                                    <PostListItem key={key} id={key} post={posts[key]} />
                                  ))
                               ) : 'Loading ...'

    return (
      <div className="blogs__list">
        {postList}
      </div>
    )

  }

}
export default connect()(PostsList)
