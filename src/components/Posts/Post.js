import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { getPosts } from '../../actions/actions';
import Menu from '../Menu';

class Post extends PureComponent {

  componentDidMount() {
    const { onGetPosts } = this.props;
    onGetPosts();
  }

  render(){
      const { post } = this.props;

      const imagePost = (post && post.image) ? (<img src={post.image} alt={post.title} className="post__image"/>) : '';

      const isLoadedPost = (post) ? (
                                      <div>
                                        <h3>
                                          {post.title}
                                        </h3>
                                        <div className="share_likes clearfix">
                                          <div className="author__post ">
                                            <small>
                                              <span>Author: </span>
                                              {post.author}
                                            </small>
                                          </div>
                                          <div className="likes">
                                            <i className="fa fa-heart"></i>
                                            <span className="love-count">{post.likes}</span>
                                          </div>
                                        </div>

                                        <div>
                                          { imagePost }
                                          {post.text.map((item, index) => <p key={index} className="content__text">{item}</p>)}
                                        </div>

                                      </div>
                                    ) : 'Loading ...';

      return (
        <div className="main">
          <Menu />
          <div className="content">
            {isLoadedPost}
          </div>
        </div>
      )
  }
};


const mapStateToProps = (state, ownProps) => {
  return {
    posts: state.posts,
    post: state.posts.find(post => post.id === Number(ownProps.params.id))
  };
}

const marDispatchToProps = (dispatch) => {
  return {
    onGetPosts: () => dispatch(getPosts()),
  }
}

export default connect(mapStateToProps, marDispatchToProps)(Post);
