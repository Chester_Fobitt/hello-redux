import React, { PureComponent } from 'react';
import Menu from '../Menu';
import { connect } from 'react-redux';

class AddPost extends PureComponent {

  render() {
    const { onAddPost } = this.props;

    let postTitle = '';
    let postDescription = '';
    let postFullText = '';
    let postAuthor = '';

    const addPost = (event) => {
      event.preventDefault();
      let idNewPost = Number(Date.now().toString());
      let newPost = {
        id: idNewPost,
        title: postTitle.value,
        description: postDescription.value,
        text: postFullText.value,
        author: postAuthor.value
      }

      onAddPost(newPost);
      postTitle.value = postAuthor.value = postDescription.value = postFullText.value = '';
    }

    return (
      <div className="main">
        <Menu />
        <div className="content">
          <form className="pure-form pure-form-aligned">
            <fieldset>
             <legend>Create new post</legend>
              <div className="">

                <div className="pure-control-group pure-g">
                  <label htmlFor="" className="pure-u-1-4">Title</label>
                  <input type="text" ref={(input)=> {postTitle = input}} className="pure-u-3-4" />
                </div>

                <div className="pure-control-group pure-g">
                  <label htmlFor="" className="pure-u-1-4">Desription</label>
                  <textarea ref={(input)=> {postDescription = input}}  className="pure-u-3-4"/>
                </div>

                <div className="pure-control-group pure-g">
                  <label htmlFor="" className="pure-u-1-4">Full text</label>
                  <textarea ref={(input)=> {postFullText = input}}  className="pure-u-3-4"/>
                </div>

                <div className="pure-control-group pure-g">
                  <label htmlFor="" className="pure-u-1-4">Author</label>
                  <input type="text" ref={(input)=> {postAuthor = input}}  className="pure-u-3-4"/>
                </div>

                <div className="pure-controls">
                  <button onClick={addPost} className="pure-button pure-button-primary">Add post</button>
                </div>
              </div>
            </fieldset>
          </form>
        </div>
      </div>
    )

  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    ...state,
    ownProps
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onAddPost: (post) => {
      dispatch({
        type: 'ADD_POST',
        payload: post
      })
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddPost);
