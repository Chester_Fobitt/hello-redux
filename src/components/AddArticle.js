import React from 'react';
import Menu from './Menu';
import { connect } from 'react-redux';

const AddArticle = ({state, onAddArticle, ownProps}) => {
  let articleTitle = '';
  let articleDescription = '';
  let articleFullText = '';
  let articleAuthor = '';

  const addArticle = (event) => {
    event.preventDefault();
    let idNewArticle = Number(Date.now().toString());
    let newArticle = {
      id: idNewArticle,
      title: articleTitle.value,
      description: articleDescription.value,
      text: articleFullText.value,
      author: articleAuthor.value
    }

    onAddArticle(newArticle);
    articleTitle.value = articleAuthor.value = articleDescription.value = articleFullText.value = '';
  }

  console.log(state);

  return (
    <div className="main">
      <Menu />
      <div className="content">
        <form className="pure-form pure-form-aligned">
          <fieldset>
           <legend>Create new blog</legend>
            <div className="">

              <div className="pure-control-group pure-g">
                <label htmlFor="" className="pure-u-1-4">Title</label>
                <input type="text" ref={(input)=> {articleTitle = input}} className="pure-u-3-4" />
              </div>

              <div className="pure-control-group pure-g">
                <label htmlFor="" className="pure-u-1-4">Desription</label>
                <textarea ref={(input)=> {articleDescription = input}}  className="pure-u-3-4"/>
              </div>

              <div className="pure-control-group pure-g">
                <label htmlFor="" className="pure-u-1-4">Full text</label>
                <textarea ref={(input)=> {articleFullText = input}}  className="pure-u-3-4"/>
              </div>

              <div className="pure-control-group pure-g">
                <label htmlFor="" className="pure-u-1-4">Author</label>
                <input type="text" ref={(input)=> {articleAuthor = input}}  className="pure-u-3-4"/>
              </div>

              <div className="pure-controls">
                <button onClick={addArticle} className="pure-button pure-button-primary">Add blog</button>
              </div>
            </div>
          </fieldset>
        </form>
      </div>
    </div>
  )
}

const mapStateToProps = (state, ownProps) => {
  return {
    ...state,
    ownProps
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onAddArticle: (blog) => {
      dispatch({
        type: 'ADD_BLOG',
        payload: blog
      })
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddArticle);
