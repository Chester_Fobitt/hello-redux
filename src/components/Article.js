import React from 'react';
import Menu from './Menu';
import { connect } from 'react-redux';

const Article = ({blog}) => {
  return (
    <div className="main">
      <Menu />
      <div className="content">
        <h3>
          {blog.title}
        </h3>
        <small>
          {blog.author}
        </small>
        <p>
          {blog.text}
        </p>
      </div>
    </div>
  )
};

const mapStateToProps = (state, ownProps) => {
  console.log(ownProps);
  return {
    blog: state.blogs.find(blog => blog.id === Number(ownProps.params.id))
  };
}

export default connect(mapStateToProps)(Article);
