import React, { PureComponent } from 'react';
import Menu from './Menu';
import { connect } from 'react-redux';
import { getPosts, getFindPosts } from '../actions/actions';
import PostsList from './Posts/PostsList'

class App extends PureComponent {

  componentDidMount() {
    const { onGetPosts } = this.props;
    onGetPosts();
  }

  render(){
    const { onFindPosts, posts } = this.props;

    let searchInput = '';
    const findPosts = () =>{
      onFindPosts(searchInput.value);

    }
    return (
      <div className="main">
        <Menu/>

        <div className="content">
          <div className="pure-form pure-form-aligned pure-g">
            <input
              type="text"
              ref={(input)=> {searchInput = input}}
              placeholder="Search"
              className="pure-u-5-5"
              onChange={findPosts}
            />
          </div>
          <PostsList posts={posts} />
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    posts: state.posts.filter(post => post.title.toLowerCase().includes(state.filterPosts))
  }
}

const marDispatchToProps = (dispatch) => {
  return {
    onFindPosts: (name) => dispatch(getFindPosts(name)),
    onGetPosts: () => dispatch(getPosts()),
  }
}

export default connect(mapStateToProps, marDispatchToProps)(App);
