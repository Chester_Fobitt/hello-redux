import React, { PureComponent } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';

class Menu extends PureComponent {

  render(){
    return (
      <div className="pure-menu pure-menu-horizontal pure-menu-scrollable pure-g">
        <div className="pure-u-17-24">
          <div className="logo"></div>
        </div>
        <ul className="pure-menu-list pure-u-7-24">
          <li className="pure-menu-item">
            <Link to="/" activeClassName="active" className="pure-menu-link">Posts</Link>
          </li>
          <li className="pure-menu-item">
            <Link to="/add" activeClassName="active" className="pure-menu-link">Add post</Link>
          </li>
          <li className="pure-menu-item">
            <Link to="/about" activeClassName="active" className="pure-menu-link">About</Link>
          </li>
        </ul>
      </div>
    )
  }
}

export default connect()(Menu);
