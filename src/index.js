import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route } from 'react-router';
import { store, history } from './store/createStore';

import App from './components/App';
import About from './components/About';
import Post from './components/Posts/Post';
import AddPost from './components/Posts/AddPost';

import './index.css';

render(
	<Provider store={store}>
		<Router history={history}>
			<Route path="/" component={App} />
			<Route path="/posts/:id" component={Post} />
			<Route path="/add" component={AddPost} />
			<Route path="/about" component={About} />
		</Router>
	</Provider>,
	document.getElementById('root')
);
